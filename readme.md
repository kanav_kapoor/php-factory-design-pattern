# Factory Design Pattern

An application for a coffee shop demostrating Factory Design Pattern.

The coffee shop sells two products "Americano" and "Espresso". They also sell addons (AlmondMilk and Soy) at an extra price.

The customers can request to add these ingredients to their coffee. Example prices:

Beverages|Price
---------|------
Americano|$2.75
Espresso|$1.75

Addons|Price
------|-----
Almond Milk|$0.75
Soy|$0.50

### Install Instructions

~~~~
 $ git clone https://kanav_kapoor@bitbucket.org/kanav_kapoor/oops-coffee-factory.git
 $ composer-dump autoload -o
~~~~

### Code Structure
 - `src` directory is autoloaded by `psr-4` autoloading using composer
 - All classes belong to the `App` namespace
 - Beverage is an abstract class having two properties i.e., description and cost
 - Americano and Espresso classes are based on Beverage
 - These classes define the cost and description for each beverage
 - Addon abstract class is based on the Beverage class.
 - Additional classes are created for additional ingredients like AlomndMilk and Soy based on Addon Class that define cost and description for each ingredient.
 
### Explanation of Code
 - `index.php` creates an Americano using the factory design pattern.
 - It creates an order for a customer who likes Americano with AlmondMilk.
 - It prints the cost and description for the order.