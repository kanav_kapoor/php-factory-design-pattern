<?php

namespace App;

use App\Addons\Addon;
use App\Beverages\Beverage;

class Order
{
    public $beverage;
    public $addons = [];

    public function __construct(Beverage $beverage)
    {
        $this->beverage = $beverage;
    }

    public function newAddon(Addon $addon)
    {
        if(!in_array($addon, $this->addons)){
            array_push($this->addons, $addon);
        }

        return $this->addons;
    }

    public function cost()
    {
        $costs = [$this->beverage->cost];

        foreach($this->addons as $addon){
            array_push($costs, $addon->cost);
        }

        return array_sum($costs);
    }

    public function description()
    {
        $descriptions = [$this->beverage->description];

        foreach($this->addons as $addon){
            array_push($descriptions, $addon->description);
        }

        return implode(' + ', $descriptions);
    }


}