<?php

namespace App;

use App\Beverages\Espresso;
use App\Beverages\Americano;

class CoffeeFactory {

    public function makeAmericano()
    {
        $americano = new Americano;
        $americano->setCost('2.75');
        $americano->setDescription('A Hot Americano Coffee');

        return $americano;
    }

    public function makeEspresso()
    {
        $espresso = new Espresso;
        $espresso->setCost('1.75');
        $espresso->setDescription('A Hot Espresso Coffee');

        return $espresso;
    }
}