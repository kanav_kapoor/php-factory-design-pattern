<?php

namespace App;

use App\Addons\Soy;
use App\Addons\AlmondMilk;

class AddonFactory {

    public function makeAlmondMilk()
    {
        $almondMilk = new AlmondMilk;
        $almondMilk->setCost('0.75');
        $almondMilk->setDescription('Almond Milk');

        return $almondMilk;
    }

    public function makeSoy()
    {
        $soy = new Soy;
        $soy->setCost('0.50');
        $soy->setDescription('Soy');

        return $soy;
    }
}