<?php

require 'vendor/autoload.php';

use App\Order;
use App\AddonFactory;
use App\CoffeeFactory;

// Creating an Americano using the factory design pattern.
$americano = (new CoffeeFactory)->makeAmericano();

// Creating an almond milk addon using the factory design pattern.
$almondMilk = (new AddonFactory)->makeAlmondMilk();

// Creating Order
$order = new Order($americano);
$order->newAddon($almondMilk);

echo "Cost of {$order->description()} is $" . $order->cost();